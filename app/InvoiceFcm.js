import React, { Component } from 'react';
import FirebaseConstants from "./FirebaseConstants";

import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Clipboard,
    Platform,
    ScrollView
} from "react-native";

import FCM, { NotificationActionType } from "react-native-fcm";

import { registerKilledListener, registerAppListener } from "./Listeners";
import firebaseClient from "./FirebaseClient";
console.disableYellowBox = true;




export default class InvoiceFcm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: "",
            tokenCopyFeedback: ""
        };

    }

    static navigationOptions = {
        header: null
    };



    async componentDidMount() {
        //FCM.createNotificationChannel is mandatory for Android targeting >=8. Otherwise you won't see any notification
        FCM.createNotificationChannel(
            {
                id: 'default',
                name: 'Default',
                description: 'used for example',
                priority: 'high'
            })

        registerAppListener(this.props.navigation);
        FCM.getInitialNotification().then(notif => {
            this.setState({
                initNotif: notif
            });
            if (notif && notif.targetScreen === "detail") {
                setTimeout(() => {
                    this.props.navigation.navigate("Detail");
                }, 500);
            }
        });

        try {
            let result = await FCM.requestPermissions({
                badge: false,
                sound: true,
                alert: true
            });
        } catch (e) {
            console.error(e);
        }

        FCM.getFCMToken().then(token => {
            console.log("TOKEN (getFCMToken)", token);
            this.setState({ token: token || "" });
        });

        if (Platform.OS === "ios") {
            FCM.getAPNSToken().then(token => {
                console.log("APNS TOKEN (getFCMToken)", token);
            });
        }

        // topic example
        // FCM.subscribeToTopic('sometopic')
        // FCM.unsubscribeFromTopic('sometopic')
    }




    showLocalNotification() {
        FCM.presentLocalNotification(
            {
                channel: 'default',
                id: new Date().valueOf().toString(), // (optional for instant notification)
                title: "Mehul", // as FCM payload
                body: "Force touch to reply", // as FCM payload (required)
                sound: "bell.mp3", // "default" or filename
                priority: "high", // as FCM payload
                click_action: "com.myapp.MyCategory", // as FCM payload - this is used as category identifier on iOS.
                badge: 10, // as FCM payload IOS only, set 0 to clear badges
                number: 10, // Android only
                ticker: "My Notification Ticker", // Android only
                auto_cancel: true, // Android only (default true)
                large_icon:
                    "https://i.picsum.photos/id/116/640/360.jpg", // Android only
                icon: "ic_launcher", // as FCM payload, you can relace this with custom icon you put in mipmap
                big_text: "Show when notification is expanded", // Android only
                sub_text: "This is a subText", // Android only
                color: "red", // Android only
                vibrate: 300, // Android only default: 300, no vibration if you pass 0
                wake_screen: true, // Android only, wake up screen when notification arrives
                group: "group", // Android only
                picture:
                    "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png", // Android only bigPicture style
                ongoing: true, // Android only
                my_custom_data: "my_custom_field_value", // extra data you want to throw
                lights: true, // Android only, LED blinking (default false)
                show_in_foreground: true // notification when app is in foreground (local & remote)

            });
    }

    sendRemoteNotification(token) {

        var bodydata = JSON.stringify(
            {
                "to": "feHDrtJ4of4:APA91bFB8bfjzVnohYAAg0SfIQB977k-ylab3vCF_ArdKSfMjus8BlCSXhKHPVsyAl-TWPIeCPZRaholh9D1-JUj74hGtajqMfk8MtM2ZOhXO0ikN5tAF8nUKUF8iAECUh78dpEeIzB8",
                // "to": token,
                "notification":
                {
                    "body": "React Native",
                    "title": "React Mehul",
                    "content_available": true,
                    "priority": "high",
                }
            })


        console.log("data" + bodydata)


        fetch('https://fcm.googleapis.com/fcm/send',
            {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    Accept: 'application/json',
                    "Authorization": "key=" + FirebaseConstants.KEY
                },
                body: bodydata
            })

            .then(response => response.json())
            .then(data => {

                console.log("datams" + JSON.stringify(data))
            })
            .catch(error => {
                console.log(error)
            });


    }


    render() {
        let { token, tokenCopyFeedback } = this.state;
        return (
            <View style={styles.container}>



                <TouchableOpacity
                    onPress={() => this.sendRemoteNotification(token)}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Send Remote Notification</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => this.showLocalNotification()}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>Show Local Notification</Text>
                </TouchableOpacity>




            </View>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    buttonText: {
        color: "black",
        backgroundColor: "transparent"
    },
    button: {
        backgroundColor: "teal",
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginVertical: 10,
        borderRadius: 10
    },
    

})